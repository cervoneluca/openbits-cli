import npm from 'npm';
import pack from 'libnpmpack';

export const installNodePackage = async (packageName, saveDev, g) => {
  const promise = new Promise((resolve) => {
    npm.load({
      loaded: false,
      'save-dev': saveDev,
      global: g,
    }, () => {
      npm.commands.install([packageName], (cb) => {
        const result = resolve(cb);
        return result;
      });
    });
  });
  return promise;
};

export const uninstallNodePackage = async (packageName, g) => {
  const promise = new Promise((resolve) => {
    npm.load({
      loaded: false,
      global: g,
    }, () => {
      npm.commands.uninstall([packageName], (cb) => {
        const result = resolve(cb);
        return result;
      });
    });
  });
  return promise;
};

export const packNodePackage = async () => {
  const tarball = await pack();
  return tarball;
};
