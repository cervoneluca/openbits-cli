import configStore from './config-store';

export const openBitCreatePSTTransaction = async (arweave, userKey, initState) => {
  // create the transaction
  const transaction = await arweave.createTransaction({
    data: JSON.stringify(initState),
  }, userKey);

  // add tags to the transaction
  transaction.addTag('dApp', 'OpenBits');
  transaction.addTag('type', 'cbvac-pst');
  transaction.addTag('App-Name', 'SmartWeaveContract');
  transaction.addTag('App-Version', '0.3.0');
  transaction.addTag('Contract-Src', configStore().get('arweaveOpenBitsCBVACPSTSource'));
  transaction.addTag('Content-Type', 'application/json');

  // sign the transaction
  await arweave.transactions.sign(transaction, userKey);
  return transaction;
};

export const publishOpenBitTransaction = async (
  arweave,
  userKey,
  openBitData,
  amount,
  receiverAddress,
  openBitName,
  openBitVersion,
  openBitCBVACPSTAddress,
) => {
  // create the transaction
  const transaction = await arweave.createTransaction({
    quantity: amount,
    target: receiverAddress,
    data: openBitData,
  }, userKey);
  // add tags to the transaction
  transaction.addTag('dApp', 'OpenBits');
  transaction.addTag('type', 'node-package');
  transaction.addTag('App-Name', 'SmartWeaveAction');
  transaction.addTag('App-Version', '0.3.0');
  transaction.addTag('Contract', configStore().get('arweaveOpenBitsRegistryAddress'));
  transaction.addTag('Input', JSON.stringify({
    function: 'register',
    type: 'node-package',
    name: openBitName,
    version: openBitVersion,
    pstId: openBitCBVACPSTAddress,
  }));

  // sign the transaction
  await arweave.transactions.sign(transaction, userKey);
  return transaction;
};
