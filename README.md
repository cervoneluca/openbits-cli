# OpenBits CLI

<div align="center">
   <img align="center" src="https://gitlab.com/cervoneluca/openbits/-/raw/master/assets/logo-black-new.svg" width="250px" alt="OpenBits logo" title="OpenBits Logo">
</div>

OpenBits is a platform based on the [Arweave](https://www.arweave.org) blockchain. Arweave is a network that incentivize users to create censorship-resistant permanent contents. **OpenBits incentivize developers to create those small packages that lay the foundation to all the complex applications we use day by day.**

Read more about the OpenBits project here: [https://www.openbits.world](https://www.openbits.world)!

## Installation

If you install the OpenBits CLI for the first time, run:

```bash
npm install -g openbits
```

If you want to update the OpenBits CLI by means of OpenBits itself, run: 

```bash
openbits install -g openbits
```


## Usage

<!-- 
   Firstly, make sure to have an arweave wallet. If you don't have one you can generate one by running: 

   ```bash
      openbits generate <output-folder-of-your-wallet-key-file>
   ```
--> 

Firstly, make sure to have an arweave wallet. If you don't have one you can generate one by visiting the [Arweave](https://www.arweave.org) project web page. 

> **Before starting to use OpenBits you have to fund your Arweave wallet with ARs (that is the arweave token). This is not a matter concerning OpenBits how you get ARs but you can easily understand how to do that by running a research on the web.** 

Having obtained an arweave wallet, you can login into OpenBits by running the following: 

```bash
openbits login <path-of-your-wallet-key-file>
```
The arweave wallet key will be stored on your machine in a location that depends to the OS you are using. For instance MacOS the wallet will be stored in ~/.config/configstore/openbits.json. When you finish to play with OpenBits, if you want to delete your wallet key file from that location, run: 

```bash
openbits logout
```

## Installing an OpenBit

Firstly, **be aware that currently OpenBits only supports Node Packages.**
Thus, if you are in a node project, and you want to install an OpenBit (a node package) by means of OpenBits, run the following: 

```bash
openbits install <name-of-the-openbit[@openbit-version]>
```

If you don't specify a version, the latest version available of the OpenBit will be installed. 
You can also install the OpenBit only for development, by adding a --save-dev flag, or you can install it globally, by adding a -g flag.

When you install an OpenBit, you can choose to contribute to make it reach its target profit. After having done its calculation OpenBits will show you the following screen shoot: 

<div align="center">
   <img align="center" src="https://gitlab.com/cervoneluca/openbits-cli/-/raw/master/assets/screenshoot-install1.png" width="70%" alt="OpenBits logo" title="Install Screen Shoot One">
</div>

If you want to pay 0.01 AR to install the OpenBit and to contribute to make it to reach its target profit, write the displayed random word (in the above case it is: NESTLIKE) and hit enter. In this case, transactions will be created and sent and the OpenBit will be installed (without waiting for confirmation of transactions).

Otherwise, if you do not want to contribute to the OpenBit funding, just write anything else and hit return (or just hit return). In this case the OpenBits will be installed anyway. 

> **❤️ Remember: either if you want to contribute to donate the OpenBit to the MULTIVERSE, or if you do not want, OPENBITS WILL LOVE YOU.❤️** 

## Publishing an OpenBit

Firstly, **be aware that currently OpenBits only supports Node Packages.**
Thus, if you are in a node project, and you want to publish on OpenBits, run the following: 

```bash
openbits publish
```

OpenBits will ask you for the profit target that you want to achieve by means of your OpenBit, and it will ask you if you want to allow investors to buy shares of your OpenBit, as displayed in the following screen shoot: 

<div align="center">
   <img align="center" src="https://gitlab.com/cervoneluca/openbits-cli/-/raw/master/assets/screenshoot-publish1.png" width="70%" alt="OpenBits logo" title="Install Screen Shoot One">
</div>

Follow the instructions that appears on your terminal and wait until everything is set up. After that, your OpenBits will be published but, however, you'll need to wait that the arweave transactions for publication are confirmed. To check the status of the publication run: 

```bash
openbits published-status <your-openbit-name@your-openbit-version>
```
## Uninstalling an OpenBit

Firstly, **be aware that currently OpenBits only supports Node Packages.**
Thus, if you are in a node project, and you want to uninstall an OpenBit (a node package) by means of OpenBits, run the following: 

```bash
openbits uninstall <name-of-the-openbit> 
```

If you want to uninstall the package globally add -g flag.  

## Developing for OpenBits

If you're interested in contributing to the OpenBits codebase. You can fork this repository and then clone it. After you fork and install the repository, run:

```bash
npm install
```

Make sure to uninstall OpenBits globally too:

```bash
npm uninstall openbits --global
yarn global remove openbits
```

Then link the project using `npm link`:

```bash
npm link
```

Confirm the OpenBits is connected locally by running:

```bash
openbits --help
```

If everything ran correctly, you can now run:

```bash
npm run dev
```

And can officially start developing new features!